package dev.a2.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {

    @Autowired
    private ShoppingCart shoppingCart;


    @GetMapping("/result")
    public List<String> result() {
        shoppingCart.getProducts().add("1");
        return shoppingCart.getProducts();
    }

}
