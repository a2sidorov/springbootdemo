package dev.a2.hello;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    List<String> products = new ArrayList<>();

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }
}
